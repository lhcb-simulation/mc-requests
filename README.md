# MC Requests

This repository can be used to create requests for simulated data.

See [here](https://lhcb-simulation.web.cern.ch/liaisons.html) for information about how to use this repository.

Recent CI pipelines can be found [here](https://lhcb-productions.web.cern.ch/simulation/pipelines/).
